package com.trip.finder.ui

import android.app.Application
import com.trip.finder.BuildConfig
import com.trip.finder.di.AppModule
import com.trip.finder.di.Scopes
import com.trip.finder.utils.SimpleLogger
import io.reactivex.plugins.RxJavaPlugins
import toothpick.Toothpick
import toothpick.configuration.Configuration

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        initToothpick()
        RxJavaPlugins.setErrorHandler { SimpleLogger.log(it) }
    }

    private fun initToothpick() {
        if (BuildConfig.DEBUG) {
            Toothpick.setConfiguration(Configuration.forDevelopment().preventMultipleRootScopes())
        } else {
            Toothpick.setConfiguration(Configuration.forProduction())
        }

        val rootScope = Toothpick.openScope(Scopes.APP_SCOPE)
        rootScope.installModules(AppModule(this))
    }
}