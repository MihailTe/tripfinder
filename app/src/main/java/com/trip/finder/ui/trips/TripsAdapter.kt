package com.trip.finder.ui.trips

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.trip.finder.R
import com.trip.finder.ui.trips.models.TripItem

class TripsAdapter(private val onItemClicked: (TripItem) -> Unit) : RecyclerView.Adapter<TripsAdapter.ViewHolder>() {

    var items: List<TripItem> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TripsAdapter.ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.list_item_trip, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val name: TextView = itemView.findViewById(R.id.hotelName)
        private val flightsAmountHint: TextView = itemView.findViewById(R.id.flightsAmountHint)
        private val minPrice: TextView = itemView.findViewById(R.id.minPrice)

        fun bind(item: TripItem) {
            val context = itemView.context
            name.text = context
                .getString(
                    R.string.trips_hotel_name_template,
                    item.hotelName
                )

            flightsAmountHint.text = context
                .resources
                .getQuantityString(
                    R.plurals.trips_flights_amount_template,
                    item.availableFlights.size,
                    item.availableFlights.size
                )

            minPrice.text = context
                .getString(
                    R.string.trips_from_price_template,
                    item.minRublePrice
                )

            itemView.setOnClickListener { onItemClicked(item) }
        }
    }
}