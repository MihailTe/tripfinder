package com.trip.finder.ui.trips.models

class TripItem(
    val hotelName: String,
    val availableFlights: List<FlightItem>,
    val minRublePrice: Int
)