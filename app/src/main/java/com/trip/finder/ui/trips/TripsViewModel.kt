package com.trip.finder.ui.trips

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.trip.finder.business.TripsInteractor
import com.trip.finder.ui.common.ScreenState
import com.trip.finder.ui.trips.models.TripItem
import com.trip.finder.ui.trips.models.TripsItemsMapper
import com.trip.finder.utils.SimpleLogger
import io.reactivex.android.schedulers.AndroidSchedulers.mainThread
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class TripsViewModel @Inject constructor(
    private val tripsInteractor: TripsInteractor,
    private val mapper: TripsItemsMapper
) : ViewModel() {

    private val screenState = MutableLiveData<ScreenState>()
    private val trips = MutableLiveData<List<TripItem>>()

    private var disposable = Disposables.empty()

    init {
        findTrips()
    }

    fun retry() {
        findTrips()
    }

    private fun findTrips() {
        screenState.value = ScreenState.LOADING

        disposable = tripsInteractor
            .findTrips()
            .map { mapper.mapTripItems(it) }
            .observeOn(mainThread())
            .subscribeBy(
                onSuccess = {
                    trips.value = it
                    screenState.value = ScreenState.CONTENT
                },
                onError = {
                    screenState.value = ScreenState.ERROR
                    SimpleLogger.log(it)
                })
    }

    override fun onCleared() {
        disposable.dispose()
    }

    fun getScreenState(): LiveData<ScreenState> = screenState

    fun getTrips(): LiveData<List<TripItem>> = trips
}