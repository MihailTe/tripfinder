package com.trip.finder.ui.trips.models

import com.trip.finder.business.TripsInteractor
import com.trip.finder.business.models.Flight
import com.trip.finder.business.models.Hotel
import com.trip.finder.business.models.Trip
import javax.inject.Inject

class TripsItemsMapper @Inject constructor(private val interactor: TripsInteractor) {

    fun mapTripItems(hotels: List<Trip>): List<TripItem> {
        return hotels
            .map { trip ->
                TripItem(
                    hotelName = trip.hotel.name,
                    availableFlights = trip.hotel.flights.map { mapFlightItem(trip.hotel, it) },
                    minRublePrice = trip.minRublePrice
                )
            }
    }

    private fun mapFlightItem(hotel: Hotel, flight: Flight): FlightItem {
        return FlightItem(
            airlineCompanyName = flight.company.name,
            tripRublePrice = interactor.calculateTripPrice(hotel, flight)
        )
    }
}