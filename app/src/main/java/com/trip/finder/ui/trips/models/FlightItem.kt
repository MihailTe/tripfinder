package com.trip.finder.ui.trips.models

class FlightItem(
    val airlineCompanyName: String,
    val tripRublePrice: Int
)