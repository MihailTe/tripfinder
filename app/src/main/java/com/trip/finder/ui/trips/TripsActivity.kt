package com.trip.finder.ui.trips

import android.arch.lifecycle.Observer
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.StyleSpan
import com.trip.finder.R
import com.trip.finder.di.provideViewModel
import com.trip.finder.ui.common.ScreenState
import com.trip.finder.ui.trips.models.TripItem
import com.trip.finder.ui.utils.appendWithSpan
import com.trip.finder.ui.utils.setDisplayedChildId
import kotlinx.android.synthetic.main.trips_activity.*

class TripsActivity : AppCompatActivity() {

    private lateinit var viewModel: TripsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel = provideViewModel(TripsViewModel::class.java)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.trips_activity)
        setupViews()
    }

    private fun setupViews() {
        tripsList.layoutManager = LinearLayoutManager(this)
        val adapter = TripsAdapter { showFlights(it) }
        tripsList.adapter = adapter

        retryButton.setOnClickListener { viewModel.retry() }

        viewModel.getScreenState()
            .observe(this, Observer {
                when (it) {
                    ScreenState.LOADING -> viewAnimator.setDisplayedChildId(R.id.progress)
                    ScreenState.CONTENT -> viewAnimator.setDisplayedChildId(R.id.tripsList)
                    ScreenState.ERROR -> viewAnimator.setDisplayedChildId(R.id.error)
                }
            })

        viewModel.getTrips()
            .observe(this, Observer {
                adapter.items = it ?: return@Observer
            })
    }

    private fun showFlights(trip: TripItem) {
        val items = arrayOfNulls<CharSequence>(trip.availableFlights.size)

        trip.availableFlights
            .forEachIndexed { index, flight ->
                items[index] = SpannableStringBuilder()
                    .append(flight.airlineCompanyName)
                    .append(" – ")
                    .appendWithSpan(
                        getString(R.string.trips_ruble_price_template, flight.tripRublePrice),
                        StyleSpan(Typeface.BOLD),
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
            }

        AlertDialog.Builder(this)
            .setItems(items, null)
            .show()
    }
}