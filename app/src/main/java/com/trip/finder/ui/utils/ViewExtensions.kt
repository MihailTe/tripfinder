package com.trip.finder.ui.utils

import android.support.annotation.IdRes
import android.text.SpannableStringBuilder
import android.widget.ViewAnimator

fun ViewAnimator.setDisplayedChildId(@IdRes id: Int) {
    for (i in 0 until childCount) {
        val child = getChildAt(i)
        if (child.id == id && i != displayedChild) {
            displayedChild = i
        }
    }
}

fun SpannableStringBuilder.appendWithSpan(text: CharSequence, what: Any, flags: Int): SpannableStringBuilder {
    val start = length
    append(text)
    setSpan(what, start, length, flags)
    return this
}