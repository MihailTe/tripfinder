package com.trip.finder.ui.common

enum class ScreenState {
    LOADING, CONTENT, ERROR
}