package com.trip.finder.utils

import android.util.Log

object SimpleLogger {

    fun log(throwable: Throwable) {
        Log.e(javaClass.simpleName, throwable.message)
    }
}