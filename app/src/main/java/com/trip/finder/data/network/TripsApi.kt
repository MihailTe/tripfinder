package com.trip.finder.data.network

import com.trip.finder.data.network.models.AirlineCompaniesResponse
import com.trip.finder.data.network.models.FlightsResponse
import com.trip.finder.data.network.models.HotelsResponse
import io.reactivex.Single
import retrofit2.http.GET

interface TripsApi {

    @GET("zqxvw")
    fun getFlights(): Single<FlightsResponse>

    @GET("12q3ws")
    fun getHotels(): Single<HotelsResponse>

    @GET("8d024")
    fun getAirlineCompanies(): Single<AirlineCompaniesResponse>
}