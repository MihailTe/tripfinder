package com.trip.finder.data.network.models

class FlightsResponse(val flights: List<Flight>?) {

    class Flight(
        val id: Long?,
        val companyId: Long?,
        val price: Int?
    )
}