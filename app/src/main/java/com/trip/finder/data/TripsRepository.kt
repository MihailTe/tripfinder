package com.trip.finder.data

import com.trip.finder.data.network.models.AirlineCompaniesResponse
import com.trip.finder.data.network.models.FlightsResponse
import com.trip.finder.data.network.models.HotelsResponse
import io.reactivex.Single

interface TripsRepository {

    fun getAllHotels(): Single<List<HotelsResponse.Hotel>>

    fun getAllFlights(): Single<List<FlightsResponse.Flight>>

    fun getAllAirlineCompanies(): Single<List<AirlineCompaniesResponse.Company>>
}