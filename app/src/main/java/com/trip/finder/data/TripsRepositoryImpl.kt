package com.trip.finder.data

import com.trip.finder.data.network.TripsApi
import com.trip.finder.data.network.models.AirlineCompaniesResponse
import com.trip.finder.data.network.models.FlightsResponse
import com.trip.finder.data.network.models.HotelsResponse
import io.reactivex.Single
import javax.inject.Inject

class TripsRepositoryImpl @Inject constructor(private val tripsApi: TripsApi) : TripsRepository {

    override fun getAllHotels(): Single<List<HotelsResponse.Hotel>> {
        return tripsApi
            .getHotels()
            .map { it.hotels.orEmpty() }
    }

    override fun getAllFlights(): Single<List<FlightsResponse.Flight>> {
        return tripsApi
            .getFlights()
            .map { it.flights.orEmpty() }
    }

    override fun getAllAirlineCompanies(): Single<List<AirlineCompaniesResponse.Company>> {
        return tripsApi
            .getAirlineCompanies()
            .map { it.companies.orEmpty() }
    }
}