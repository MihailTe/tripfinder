package com.trip.finder.data.network

import com.trip.finder.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class TripsApiProvider @Inject constructor() : Provider<TripsApi> {

    override fun get(): TripsApi {
        val httpClient = OkHttpClient
            .Builder()
            .maybeAddLoggingInterceptor()
            .build()

        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .baseUrl(API_URL)
            .build()
            .create(TripsApi::class.java)
    }

    private fun OkHttpClient.Builder.maybeAddLoggingInterceptor(): OkHttpClient.Builder {
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
            addInterceptor(interceptor)
        }

        return this
    }


    companion object {
        private const val API_URL = "https://api.myjson.com/bins/"
    }
}