package com.trip.finder.data.network.models

class AirlineCompaniesResponse(val companies: List<Company>?) {

    class Company(
        val id: Long?,
        val name: String?
    )
}