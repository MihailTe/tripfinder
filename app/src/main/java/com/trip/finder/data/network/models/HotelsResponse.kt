package com.trip.finder.data.network.models

class HotelsResponse(val hotels: List<Hotel>?) {

    class Hotel(
        val id: Long?,
        val flights: List<Long>?,
        val name: String?,
        val price: Int?
    )
}