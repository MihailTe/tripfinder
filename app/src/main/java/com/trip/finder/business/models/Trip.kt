package com.trip.finder.business.models

class Trip(
    val hotel: Hotel,
    val minRublePrice: Int
)