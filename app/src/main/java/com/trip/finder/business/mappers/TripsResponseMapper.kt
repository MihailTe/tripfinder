package com.trip.finder.business.mappers

import com.trip.finder.business.models.AirlineCompany
import com.trip.finder.business.models.Flight
import com.trip.finder.business.models.Hotel
import com.trip.finder.data.network.models.AirlineCompaniesResponse
import com.trip.finder.data.network.models.FlightsResponse
import com.trip.finder.data.network.models.HotelsResponse
import javax.inject.Inject

class TripsResponseMapper @Inject constructor() {

    fun mapHotels(response: List<HotelsResponse.Hotel>, relatedFlights: List<Flight>): List<Hotel> {
        return response
            .mapNotNull { hotel ->
                val flights = (hotel.flights ?: return@mapNotNull null)
                    .map { flightId ->
                        relatedFlights.find { it.id == flightId } ?: return@mapNotNull null
                    }

                Hotel(
                    id = hotel.id ?: return@mapNotNull null,
                    flights = flights,
                    name = hotel.name ?: return@mapNotNull null,
                    rublePrice = hotel.price ?: return@mapNotNull null
                )
            }
    }

    fun mapFlights(response: List<FlightsResponse.Flight>, companies: List<AirlineCompany>): List<Flight> {
        return response
            .mapNotNull { flight ->
                val company = companies.find { it.id == flight.companyId } ?: return@mapNotNull null

                Flight(
                    id = flight.id ?: return@mapNotNull null,
                    company = company,
                    rublePrice = flight.price ?: return@mapNotNull null
                )
            }
    }

    fun mapAirlineCompanies(response: List<AirlineCompaniesResponse.Company>): List<AirlineCompany> {
        return response
            .mapNotNull {
                AirlineCompany(
                    id = it.id ?: return@mapNotNull null,
                    name = it.name ?: return@mapNotNull null
                )
            }
    }
}