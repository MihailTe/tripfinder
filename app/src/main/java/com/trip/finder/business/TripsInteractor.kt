package com.trip.finder.business

import com.trip.finder.business.mappers.TripsResponseMapper
import com.trip.finder.business.models.Flight
import com.trip.finder.business.models.Hotel
import com.trip.finder.business.models.Trip
import com.trip.finder.data.TripsRepository
import io.reactivex.Single
import io.reactivex.rxkotlin.Singles
import javax.inject.Inject

class TripsInteractor @Inject constructor(
    private val repository: TripsRepository,
    private val mapper: TripsResponseMapper
) {

    fun findTrips(): Single<List<Trip>> {
        return Singles
            .zip(
                repository.getAllHotels(),
                repository.getAllFlights(),
                repository.getAllAirlineCompanies()
            ) { hotels, flights, companies ->
                val mappedCompanies = mapper.mapAirlineCompanies(companies)
                val mappedFlights = mapper.mapFlights(flights, mappedCompanies)
                val mappedHotels = mapper.mapHotels(hotels, mappedFlights)

                mappedHotels
                    .map { Trip(it, calculateMinTripPrice(it)) }
            }
    }

    private fun calculateMinTripPrice(hotel: Hotel): Int =
        hotel.flights.minBy { it.rublePrice }?.rublePrice!! + hotel.rublePrice

    fun calculateTripPrice(hotel: Hotel, flight: Flight): Int = hotel.rublePrice + flight.rublePrice
}