package com.trip.finder.business.models

class Hotel(
    val id: Long,
    val flights: List<Flight>,
    val name: String,
    val rublePrice: Int
)