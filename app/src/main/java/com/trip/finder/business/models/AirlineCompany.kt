package com.trip.finder.business.models

class AirlineCompany(
    val id: Long,
    val name: String
)