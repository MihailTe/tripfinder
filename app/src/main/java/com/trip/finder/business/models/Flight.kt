package com.trip.finder.business.models

class Flight(
    val id: Long,
    val company: AirlineCompany,
    val rublePrice: Int
)