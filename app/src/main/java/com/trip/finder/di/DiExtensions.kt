package com.trip.finder.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import toothpick.Scope
import toothpick.Toothpick

private fun getRootScope(): Scope = Toothpick.openScopes(Scopes.APP_SCOPE)

fun <T : ViewModel> AppCompatActivity.provideViewModel(viewModelClass: Class<T>) =
    ViewModelProviders.of(this, ViewModelProviderFactory(getRootScope()))
        .get(viewModelClass)