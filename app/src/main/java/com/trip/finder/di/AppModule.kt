package com.trip.finder.di

import android.app.Application
import android.content.Context
import com.trip.finder.data.TripsRepository
import com.trip.finder.data.TripsRepositoryImpl
import com.trip.finder.data.network.TripsApi
import com.trip.finder.data.network.TripsApiProvider
import toothpick.config.Module

class AppModule(application: Application) : Module() {

    init {
        bind(Context::class.java).toInstance(application)
        bind(TripsApi::class.java).toProvider(TripsApiProvider::class.java).singletonInScope()
        bind(TripsRepository::class.java).to(TripsRepositoryImpl::class.java)
    }
}