package com.trip.finder.di

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import toothpick.Scope

class ViewModelProviderFactory(private val scope: Scope) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return scope.getInstance(modelClass)
    }
}